function click() {
    var a = document.getElementById('value1').value;
    var b = document.getElementById('value2').value;
    var operation = document.getElementById('combo').value;
    document.getElementById('result').value = calculate(operation, a, b) || 0;
}

function calculate(operation, val1, val2) {
   if (!validate(val1) || !validate(val2))
        return;
    var result,
        a = transform(val1),
        b = transform(val2);
    switch (operation) {
        case 'addition':
            result = addition_for_array(a, b);
            break;
        case 'multiply':
            result = multiply_for_array(a, b);
            break;
        case 'substraction':
            result = substraction_for_array(a, b);
            break;
    }
    return result.toString().replace(/,/g, '');
}

function validate(str_value) {
    // проверка на неотрицательное число
    return /^\d+$/.test(str_value.trim())
}

function transform(str_value) {
    // преобразование строки в массив цифр
    return str_value.trim().replace(/^0+(?=0|\d)/g, '').split('').map(x => parseInt(x));
}

Array.dim = function (dimension, initial) {
    var a = [];
    for (var i = 0; i < dimension; i++)
        a[i] = initial;
    return a;
}

function substraction_for_array(val1, val2) {
    // добавляем незначащие нули в меньшее число
    var [a, b] = align_arrays(val1, val2);

    var negative = false; // результат отрицательный
    if (is_need_swap(a, b)) {
        [a, b] = [b, a]; // меняем местами массивы
        negative = true;
    }

    var overflow = false, result = [];
    for (var i = a.length - 1; i >= 0; i--) {
        if (a[i] < b[i] + (+overflow)) {
            result[i] = a[i] + 10 - b[i]  - (+overflow);
            overflow = true;
            continue;
        }
        result[i] = a[i] - b[i]  - (+overflow);
        overflow = false;
    }

    // стираем незначащие нули, если образовались
    result = trim_noughts(result);

    if (result.length == 0)
        return [0];

    if (negative)
        result.unshift('-');

    return result;
}

function addition_for_array(val1, val2) {
    var [a, b] = align_arrays(val1, val2);
    
    if (is_need_swap)
        [a, b] = [b, a];

    var overflow = false, result = [], sum;
    for (var i = a.length - 1; i >= 0; i--) {
        sum = a[i] + b[i]  + (+overflow);
        result[i] = sum % 10;
        overflow = sum > 9;
    }
    if (overflow)
        result.unshift(1);

    return result;
}

function multiply_for_array(val1, val2) {
    var [a, b] = align_arrays(val1, val2);

    if (is_need_swap)
        [a, b] = [b, a];

    var result = [0];
    for (var i = b.length - 1; i >= 0; i--) {
        var intermediate_result = [0];
        for (var j = 0; j < b[i]; j++)
            intermediate_result = addition_for_array(a, intermediate_result);

        for (var k = 0; k < b.length - 1 - i; k++)
            intermediate_result.push(0);
        
        if (intermediate_result.some(x => x != 0))
            result = addition_for_array(result, intermediate_result);
    }

    return result;
}

function align_arrays(val1, val2) {
    var a = val1,
        b = val2,
        difference = Math.abs(val1.length - val2.length);
 
    if (val1.length < val2.length) 
        a = [].concat(Array.dim(difference, 0), val1);
    else 
        b = [].concat(Array.dim(difference, 0), val2);
    
    return [a, b];
}

function is_need_swap(a, b) {
    for (var i = 0; i < a.length; i++) 
        if (a[i] < b[i]) 
            return true;

    return false;
}

function trim_noughts(array) {
    var result = array.slice();
    while (result[0] == 0) {
        result.shift();
    }
    return result;
}